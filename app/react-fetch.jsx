/* eslint-disable no-console */
"use strict";

const {useState} = React;

function TextInput( { valueSetter } ){
    function searchUpdate(e)
    {
        let ability = e.target.value;
        ability = ability.toLowerCase();
        valueSetter(ability.replace(" ", "-"));
    }
    return <input type="text"
        onInput={ searchUpdate } />;
}

function Search( {listFetcher, setCurrentSearch} ) {
    return (
        <form>
            <TextInput valueSetter={setCurrentSearch}/>
            <button onClick={listFetcher}>Search</button>
        </form>);
}

function ResultsList( {latestResults, pokemonSelector} ) {
    return (
        <ul>
            { latestResults.map(pokemon => <li key = {pokemon.name} onClick={pokemonSelector}>{
            pokemon.name}</li>) }
        </ul>
    )
}

function Abilities( { pokemonData} )
{
    return(
        <ul>
            { pokemonData.abilities.map(ability => <li key = {ability.ability.name}>
                {ability.ability.name.toUpperCase()}</li>) }
        </ul>
    )
}

function PokemonDisplay( { pokemonData } )
{
    if (pokemonData){
        let name = pokemonData.name.toUpperCase();
        let imageSRC = pokemonData.sprites.other['official-artwork']['front_default'];
        return (
            <div>
                <h2>{name}</h2>
                <div className="pokemonContainer">
                    <div>
                        <h3>Base stats:</h3>
                        <ul>
                            { pokemonData.stats.map(stat => <li key = {stat.stat.name}>{
                                `${stat.stat.name.toUpperCase()}: ${stat.base_stat}`}</li>) }
                        </ul>
                        <h3>Abilities:</h3>
                        <Abilities pokemonData={pokemonData}/>
                    </div>
                    <article>
                        <img src={imageSRC}/>
                    </article>
                </div>
            </div>
            );  
    }
    
    return(
        <h2>Please search for an ability and select a pokemon to your left</h2>
    )
}

function App() {

    const [currentSearch, setCurrentSearch] = useState("");
    const [latestResults, setLatestResults] = useState([]);
    const [currentItem, setCurrentItem] = useState();

    function retrievePokemonList(e)
    {
        e.preventDefault();
        fetch(
            `https://pokeapi.co/api/v2/ability/${currentSearch}`, {})
            .then( response => { 
                if( !response.ok ) { 
                    throw new Error("Not 2xx response", {cause: response});
                }
                
                return response.json();
            })
            .then( obj => {
                const pokemon = obj.pokemon.map(e => e.pokemon)
                if (pokemon != null){
                    setLatestResults(pokemon);
                }
            })
            .catch( err => {
                console.error("3)Error:", err);
            });
    }

    function selectPokemon(e){
        let pokemonURL = latestResults.reduce( (acc, cur) => {
                if (cur.name === e.target.textContent){
                    acc = cur;
                }
                return acc;
            }, "").url;

        fetch(pokemonURL, {})
            .then( response => { 
                if( !response.ok ) { 
                    throw new Error("Not 2xx response", {cause: response});
                }
                return response.json();
            })
            .then( obj => {
                setCurrentItem(obj);
            })
            .catch( err => {
                console.error("3)Error:", err);
            });
    }

    return (
        <div className="wrapper">
            <header>
                <h1>Pokemon Finder</h1>
                    <p>Search for a pokemon's by their ability here:</p>
                    <p>P.S. If you don't know any abilities, try some like defiant, weak armor, or intimidate.</p>
                    <Search listFetcher={retrievePokemonList} setCurrentSearch={setCurrentSearch}></Search>
                <ResultsList latestResults = {latestResults} pokemonSelector = {selectPokemon}/>
            </header>
            <main>
                <PokemonDisplay pokemonData={currentItem}/>
            </main>
        </div> );
}

ReactDOM.render(
    <App />,
    document.querySelector("#react-root")
);
