# 440 Assignment 5

## Nathan Bokobza
## Assignment 5: deploying our own container
## The container is a finder that lets the user search for pokemon based on their ability
## instructions
    build:  docker build -t 320pokemon .
    run:    docker run -dit --name my-running-app -p 8088:80 320pokemon
