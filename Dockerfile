#deploying with httpd
FROM httpd:2.4

#maintaner/creator
LABEL maintainer="Nathan Bokobza" modified="24-05-2024"

#copy the correct files to the right location
WORKDIR /app
COPY ./app/ /usr/local/apache2/htdocs/
